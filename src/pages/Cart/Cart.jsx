import React, { useContext } from "react";
import { useSelector } from "react-redux";
import { FormBuy } from "../../components/FormBuy/FormBuy";
import { Product } from "../../components/Product/Product";
import { ToggleProductsViewContext } from "../../contexts/contexts";
import { selectorGetProducts } from "../../redux/selectors/selectors";
import styles from "./Cart.module.scss";

export const Cart = () => {
  //get toggleViewProducts from context
  const { toggleView } = useContext(ToggleProductsViewContext);
  const classView = toggleView ? styles.grid : styles.layout;

  // get products from Cart
  const { items, productsInCart } = useSelector(selectorGetProducts);

  // set products to Cart
  const productsCart = items.filter((product) =>
    productsInCart.find((item) => item.id === product.id)
  );

  // check flag if Cart is empty
  const emptyCart = !productsCart.length;

  return (
    <div className={styles.Cart}>
      {emptyCart && <h2>There are no products in the cart</h2>}
      {!emptyCart && <FormBuy productsCart={productsCart} />}
      {!emptyCart && (
        <ul className={[styles.CartList, classView].join(" ")}>
          {productsCart.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};
