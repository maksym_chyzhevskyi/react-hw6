import React, { useContext } from "react";
import { useSelector } from "react-redux";
import { Product } from "../../components/Product/Product";
import { ToggleProductsViewContext } from "../../contexts/contexts";
import { selectorGetProducts } from "../../redux/selectors/selectors";
import styles from "./Shop.module.scss";

export const Shop = () => {
  // get toggleViewProducts from context
  const { toggleView } = useContext(ToggleProductsViewContext);
  const classView = toggleView ? styles.grid : styles.layout;

  // get status upload products
  const { error, status, items } = useSelector(selectorGetProducts);

  return (
    <div className={styles.Shop}>
      {status === "pending"}
      {error && <h2>{error}</h2>}

      {!error && status === "fulfilled" && (
        <ul className={[styles.ShopList, classView].join(" ")}>
          {items.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};
