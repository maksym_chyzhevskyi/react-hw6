import React from "react";

import { GoodsSection } from "../GoodsSection/GoodsSection";

import styles from "./Main.module.scss";

export const Main = () => {
  return (
    <div className={styles.Main__row}>
      <GoodsSection />
    </div>
  );
};
