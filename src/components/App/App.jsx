import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Layout } from "../../Layout/Layout";
import { getData } from "../../redux/extraReducers/getData";
import {
  setFavorites,
  setProductsInCart,
} from "../../redux/reducers/productsThunk";
import { ToggleProductsViewProvider } from "../../contexts/ToggleProductsViewProvider";

const favoriteIdsFromStorage = localStorage.getItem("favorites");

const cartIdsFromStorage = localStorage.getItem("Cart");

export const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getData());
  }, [dispatch]);

  useEffect(() => {
    if (favoriteIdsFromStorage) {
      dispatch(setFavorites(JSON.parse(favoriteIdsFromStorage)));
    }

    if (cartIdsFromStorage) {
      dispatch(setProductsInCart(JSON.parse(cartIdsFromStorage)));
    }
  }, [dispatch]);

  return (
    <ToggleProductsViewProvider>
      <Layout />
    </ToggleProductsViewProvider>
  );
};
