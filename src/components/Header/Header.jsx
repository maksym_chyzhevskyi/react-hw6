import React, { useContext } from "react";
import { useSelector } from "react-redux";
import { selectorGetProducts } from "../../redux/selectors/selectors";
import { CartIco } from "../../UI/Icons/CartIco/cartIco";
import { FavIco } from "../../UI/Icons/FavIco/favIco";
import { GridIco } from "../../UI/Icons/GridIco/gridIco";
import { LayoutIco } from "../../UI/Icons/LayoutIco/layoutIco";
import { HeaderMenu } from "../../UI/Navigation/HeaderMenu/HeaderMenu";
import { ToggleProductsViewContext } from "../../contexts/contexts";
import styles from "./Header.module.scss";
import { NavLink } from "react-router-dom";

export const Header = () => {
  // get toggleViewProducts from context
  const { toggleView, setToggleView } = useContext(ToggleProductsViewContext);

  // set toggleViewProducts
  const handleToggleView = (e) => {
    setToggleView(!toggleView);
  };

  //get Favorites, productsInCart
  const { favorites, productsInCart } = useSelector(selectorGetProducts);

  // set count Favorites and Cart
  const countFavorites = favorites.length;
  const countProductsInCart = productsInCart.reduce(
    (value, item) => value + item.count,
    0
  );

  return (
    <div className={styles.Header__row}>
      <HeaderMenu />

      <div className={styles.HeaderActions}>
        <div className={styles.ViewSwitch}>
          {!toggleView && (
            <span className={styles.ViewLayout} onClick={handleToggleView}>
              <LayoutIco width={26} fill={"#861c3c"} />
            </span>
          )}
          {toggleView && (
            <span className={styles.ViewGrid} onClick={handleToggleView}>
              <GridIco width={26} fill={"#861c3c"} />
            </span>
          )}
        </div>
        <div className={styles.HeaderCart}>
          <NavLink to="/Cart">
            <CartIco width={26} fill={"#1c8646"} />
          </NavLink>
          <span className={styles.cartValue}>{countProductsInCart}</span>
        </div>
        <div className={styles.HeaderFavorites}>
          <NavLink to="/Favorites">
            <FavIco width={26} fill={"#ffda12"} />
          </NavLink>
          <span className={styles.favoritesValue}>{countFavorites}</span>
        </div>
      </div>
    </div>
  );
};
