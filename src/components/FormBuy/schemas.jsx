import * as yup from "yup";

export const validationSchema = yup.object().shape({
  name: yup
    .string()
    .matches(/^[a-zA-Za]+$/, "Enter only Latin letters")
    .required("This is a required field"),
  lastName: yup
    .string()
    .matches(/^[a-zA-Zа ]+$/, "Enter only Latin letters")
    .required("This is a required field"),
  age: yup
    .number()
    .typeError("The field must contain a number")
    .min(18, "Age must be over 18")
    .required("This is a required field"),
  address: yup.string().required("This is a required field"),
  phone: yup
    .string()
    .matches(
      /^\(\d{3}\)\d{3}-\d{2}-\d{2}$/,
      "The phone number must be in the format (###)###-##-##"
    )
    .required("This is a required field"),
});
