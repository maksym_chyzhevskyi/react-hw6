import React from "react";
import { Outlet } from "react-router-dom";
import styles from "./GoodsSection.module.scss";

export const GoodsSection = () => {
  return (
    <div className={styles.GoodsSection}>
      <Outlet />
    </div>
  );
};
