import { createBrowserRouter } from "react-router-dom";
import { App } from "../components/App/App";
import { Cart } from "../pages/Cart/Cart";
import { Favorites } from "../pages/Favorites/Favorites";
import { Shop } from "../pages/Shop/Shop";

export const router = createBrowserRouter([
  {
    element: <App />,
    path: "/",
    children: [
      {
        element: <Shop />,
        index: true,
      },
      {
        element: <Cart />,
        path: "Cart",
      },
      {
        element: <Favorites />,
        path: "favorites",
      },
    ],
  },
]);
