import { createSlice } from "@reduxjs/toolkit";
import { getData } from "../extraReducers/getData";
import { handleFavoritesClick } from "../extraReducers/handleFavoritesClick";

// default values for state products
export const initialState = {
  items: [],
  favorites: [],
  productsInCart: [],
  error: null,
  status: null,
};

const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    // set Favorites
    setFavorites: (state, action) => {
      state.favorites = action.payload;
    },
    // set Product in Cart
    setProductsInCart: (state, action) => {
      state.productsInCart = action.payload;
    },
    // add/remove Favorites id
    toggleFavorites: (state, action) => {
      if (state.favorites.includes(action.payload)) {
        state.favorites = state.favorites.filter((id) => id !== action.payload);
      } else {
        state.favorites.push(action.payload);
      }
    },

    // add productId in Cart
    handleAddCartClick: (state, action) => {
      //search for Product with id equal to action.payload
      const itemFound = state.productsInCart.some(
        (item) => item.id === action.payload
      );
      // if this Product exists
      if (itemFound) {
        state.productsInCart.find(
          (item) => item.id === action.payload
        ).count += 1;
      } else {
        state.productsInCart.push({ id: action.payload, count: 1 });
      }
      // set the Cart when it changes
      localStorage.setItem("Cart", JSON.stringify(state.productsInCart));
    },

    // delete productId in Cart
    handleDeleteCartClick: (state, action) => {
      // search for index Product with id equal to action.payload
      const itemIndex = state.productsInCart.findIndex(
        (item) => item.id === action.payload
      );
      // if this index exists
      if (itemIndex !== -1) {
        // if count > 1  we minus do 1
        if (state.productsInCart[itemIndex].count > 1) {
          state.productsInCart[itemIndex].count -= 1;
        } else {
          // if count <= 1 we deleting Product from Cart
          state.productsInCart.splice(itemIndex, 1);
        }
        // set the Cart when it changes
        localStorage.setItem("Cart", JSON.stringify(state.productsInCart));
      }
    },
  },

  extraReducers: (builder) => {
    builder.addCase(getData.rejected, (state, action) => {
      state.error = action.payload;
      state.status = "rejected";
    });
    builder.addCase(getData.pending, (state, action) => {
      state.error = null;
      state.status = "pending";
    });
    builder.addCase(getData.fulfilled, (state, action) => {
      state.error = null;
      state.status = "fulfilled";
      state.items = action.payload;
    });
    builder.addCase(handleFavoritesClick.fulfilled, (state, action) => {});
    builder.addCase(handleFavoritesClick.rejected, (state, action) => {
      console.log("error", action.payload);
    });
  },
});

export const {
  setFavorites,
  setProductsInCart,
  handleAddCartClick,
  handleDeleteCartClick,
  toggleFavorites,
} = productsSlice.actions;
export default productsSlice.reducer;
