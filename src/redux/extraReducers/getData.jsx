import { createAsyncThunk } from "@reduxjs/toolkit";

const url = "/api/ajax.json";

export const getData = createAsyncThunk(
  "products/getData",
  async (_, { rejectedWithValue }) => {
    try {
      const response = await fetch(url);
      if (!response.ok) throw new Error("Error loading products from server");
      const data = await response.json();
      return data;
    } catch (e) {
      return rejectedWithValue(e.message);
    }
  }
);
