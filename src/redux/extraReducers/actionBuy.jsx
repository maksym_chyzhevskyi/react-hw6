import { createAsyncThunk } from "@reduxjs/toolkit";
import { setProductsInCart } from "../reducers/productsThunk";

export const actionBuy = createAsyncThunk(
  "FormBuy/actionBuy",
  async (action, { getState, dispatch, rejectWithValue }) => {
    try {
      const { name, lastName, age, address, phone, items } = action;

      // report sell
      console.log("name:", name);
      console.log("lastName:", lastName);
      console.log("age:", age);
      console.log("address:", address);
      console.log("phone:", phone);
      console.log("BUY:");
      const price = items.reduce((totalPrice, item, index) => {
        console.log(`${index + 1}. ${item.name} - ${item.count} pcs`, item);
        return totalPrice + item.price * item.count;
      }, 0);
      console.log(`TOTAL: ${price} usd.`);

      // clear localStorage
      localStorage.setItem("Cart", JSON.stringify([]));

      // clear products in Cart from products reducer
      dispatch(setProductsInCart([]));

      return {
        status: true,
        name,
        lastName,
        age,
        address,
        phone,
        items,
      };
    } catch (e) {
      return rejectWithValue(e.message);
    }
  }
);
